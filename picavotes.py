#!/usr/bin/env python
# coding=utf-8

# This code is delivered under the gplv3 license
# It's purpose is to determine automatically the results of all votes in the
# ToutDouxCratie channel of Picasoft's Mattermost instance

# Author : Baptiste Wojtkowski & Kyane Pichou 
# Modified by Quentin Duchemin (2022/05/22)
# contact : baptiste@wojtkowski.fr

"""Get toutdouxcratie stats"""

# Imports
from os import linesep
from datetime import datetime
import re
import argparse
from getpass import getpass
from mattermostdriver import Driver
from statistics import mean

# Constants
ORDINARY_OUTPUT_FILE = "action-ordinaire.md"
EXTRAORDINARY_OUTPUT_FILE = "action-extraordinaire.md"
ACTION_EXTRAORDINAIRE = "#action-extraordinaire"
ACTION_ORDINAIRE = "#action-ordinaire"
ALL_MENTION = "@all"

FOR_EMOJIS = [
    "+1",
    "ok_hand"
]
AGAINST_EMOJIS = [
    "-1"
]
NEUTRAL_EMOJIS = [
    "woman_shrugging",
    "man_shrugging",
    "white_flag",
    "hourglass_flowing_sand",
    "mask"
]

MATTERMOST_DOMAIN = "team.picasoft.net"
MATTERMOST_TEAM = "picasoft"
MATTERMOST_CHANNEL = "toutdouxcratie"
MATTERMOST_FIRST_DATE = '2018-01-01'  # Date where the script will start to get votes, get it there
MATTERMOST_FIRST_TIMESTAMP = int(datetime.strptime(MATTERMOST_FIRST_DATE, '%Y-%m-%d').timestamp() * 1000)

def mattermost_login(username, password):
    """
    Login to mattermost server.
    :param username: username to use
    :param password: password for the user
    :return mm_driver: Mattermost Driver object for the API.
    """
    mm_driver = Driver({
        "url": MATTERMOST_DOMAIN,
        "scheme": "https",
        "port": 443,
        "login_id": username,
        "password": password,
    })
    mm_driver.login()

    return mm_driver


def get_posts(mm_driver):
    """
    Get all posts from the votes channel, sorted by date.
    :param mm_driver: Mattermost Driver object to query API
    :return posts: List of posts objects
    """
    # Get channel ID
    chan_id = mm_driver.channels.get_channel_by_name_and_team_name(MATTERMOST_TEAM, MATTERMOST_CHANNEL)['id']

    # Get all posts from the vote channel
    posts = [post for _, post in mm_driver.posts.get_posts_for_channel(
        chan_id, {"since": MATTERMOST_FIRST_TIMESTAMP})["posts"].items()]

    # Sort posts by creation date
    posts.sort(key=lambda post: post["create_at"])

    return posts


def get_author_name(mm_driver, user_id):
    """
    Get the first + last name of a Mattermost user based on the user ID.
    :param mm_driver: Mattermost Driver object to query API
    :param user_id: Mattermost user ID
    :return user_name: User first name + last name
    """
    user = mm_driver.users.get_user(user_id)
    formatted_name = user["first_name"] + " " + user["last_name"]
    if formatted_name == " ":
        return user["username"]
    else:
        return formatted_name


def is_vote(post):
    """
    Check if a post is a vote
    :param post: Post Mattermost object
    :return boolean:
    """
    return re.match(r".*#action.*", post["message"]) is not None or re.match(r".*#decision.*", post["message"]) is not None


def someone_join(post):
    """
    Check if the post is a system message for someone joining
    :param post: Post Mattermost object
    :return boolean:
    """
    return post["type"] == "system_add_to_channel" or post["type"] == "system_join_channel"


def someone_leave(post):
    """
    Check if the post is a system message for someone leaving
    :param post: Post Mattermost object
    :return boolean:
    """
    return post["type"] == "system_leave_channel" or post["type"] == "system_remove_from_channel"


def get_new_user_id(mm_driver, post):
    """
    Get the id of the newly added user
    :param mm_driver: Mattermost Driver object to query API
    :param post: Post Mattermost object
    :return userid: Mattermost user ID
    """
    if 'addedUserId' in post['props']:
        return post['props']['addedUserId']
    # When someone create the channel or invite himself, there is only the "username" property
    return mm_driver.users.get_user_by_username(post['props']['username'])['id']


def get_leaving_user_id(mm_driver, post):
    """
    Get the id of the user who leaved
    :param mm_driver: Mattermost Driver object to query API
    :param post: Post Mattermost object
    :return userid: Mattermost user ID
    """
    # If user is kicked
    if 'removedUserId' in post['props']:
        return post['props']['removedUserId']
    # If user leave by himself
    return mm_driver.users.get_user_by_username(post['props']['username'])['id']

"""
Removes type of action hashtag/at-all for readability,
optionnaly cutting the message for a summary.
"""
def get_main_message(message, length = -1):
    mod_msg = message.replace(ACTION_EXTRAORDINAIRE, '').replace(ACTION_ORDINAIRE, '').replace(ALL_MENTION, '').strip()
    return mod_msg[:length]

def main():
    """Run the main function."""
    # Create parser for CLI arguments
    parser = argparse.ArgumentParser(description="Get data from Picasoft's toutdouxcratie Mattermost channel.")
    subparsers = parser.add_subparsers(dest='cmd')
    parser.add_argument('--username', help="Username to connect to Mattermost API")
    parser.add_argument('--password', help="Password to connect to Mattermost API")
    parser.add_argument('--debug', action="store_true", help="Show errors messages and ignored votes")
    # Create sub command for stats
    subparsers.add_parser('stats', help='Get votes stats [default]')
    # Create sub command for export
    subparsers.add_parser('export', help='Export reports to markdown files')
    # Create sub command to list people who don't vote
    blame_parser = subparsers.add_parser('blame', help='List participation rate for everyone for the n last decisions')
    blame_parser.add_argument('--count', help="Number of votes in the past that must be considered")

    args = parser.parse_args()
    # Check which command to run
    if args.cmd is None:
        command = "stats"
    else:
        command = args.cmd

    # Ask for user and/or password if necessary
    if args.username is None:
        mm_username = input('Enter Mattermost username:')
    else:
        mm_username = args.username
    if args.password is None:
        mm_password = getpass(prompt='Enter Mattermost password for user "{}":'.format(mm_username))
    else:
        mm_password = args.password

    # Connect to Mattermost server
    mm_driver = mattermost_login(mm_username, mm_password)

    # Get all posts from the vote channel
    posts = get_posts(mm_driver)

    # Initialize some counters
    people_count = 0  # Count people in channel
    extraordinary_count = 0  # Count extraordinary votes
    ordinary_count = 0  # Count ordinary votes

    # Write Markdown files headers
    if command == "export":
        with open(EXTRAORDINARY_OUTPUT_FILE, "w") as file_object:
            file_object.write("| ID | Objet | Meneur.se | Date | Pour | Contre | Lien | \n" +
                              "| --- | --------------------------- | ---- | ---- | ---- | ------ | ---- |\n")
        with open(ORDINARY_OUTPUT_FILE, "w") as file_object:
            file_object.write("| ID | Objet | Meneur.se | Date | Pour | Contre | Lien | \n" +
                              "| --- | --------------------------- | ---- | ---- | ---- | ------ | ---- |\n")

    if command == "blame":
        # Track people who can vote
        users = {}
        # Get the first post timestamp we want to consider
        first_vote_time = [post for post in posts if is_vote(
            post) and "reactions" in post["metadata"].keys()][-int(args.count):][0]["create_at"]

        # Loop on posts
        for post in posts:
            # Track people
            if someone_join(post):
                new_user_id = get_new_user_id(mm_driver, post)
                if new_user_id in users.keys():
                    users[new_user_id]['present'] = True
                else:
                    users[new_user_id] = {
                        "present": True,
                        "participations": 0,
                        "eligible_votes": 0
                    }
                continue
            if someone_leave(post):
                leaving_user_id = get_leaving_user_id(mm_driver, post)
                users[leaving_user_id]['present'] = False
                continue

            # Check if the message is before the first vote to count
            if post["create_at"] < first_vote_time:
                continue
            # Check if it's a vote. Ignore other messages
            if not is_vote(post):
                continue
            # Drop messages without reactions
            if "reactions" not in post["metadata"].keys():
                continue

            # Count who voted
            voters = []
            # Loop on reactions
            for reaction in post["metadata"]["reactions"]:
                # Skip already count people
                if reaction["user_id"] in voters:
                    continue
                if reaction["emoji_name"] in FOR_EMOJIS + AGAINST_EMOJIS + NEUTRAL_EMOJIS:
                    # Count the vote
                    if reaction["user_id"] not in users.keys():
                        users[reaction["user_id"]] = {
                            "present": True,
                            "participations": 0,
                            "eligible_votes": 0
                        }
                    users[reaction["user_id"]]['participations'] += 1
                    voters.append(reaction["user_id"])
            # Count everybody who were present during the vote
            for user in users:
                if users[user]["present"]:
                    users[user]["eligible_votes"] += 1

        # Show stats
        for user in users:
            # Skip users that where not present during any vote
            if users[user]["eligible_votes"] == 0 or not users[user]["present"]:
                continue
            user_name = get_author_name(mm_driver, user)
            line = user_name + \
                " : " + str((100 * users[user]["participations"]) / users[user]["eligible_votes"]) + "%"
            print(line)
    else:
        n_actions = 0
        n_action_neutral = 0
        n_action_against = 0
        n_action_against_with_no_majority = 0
        n_action_ord = 0
        n_action_ext = 0
        n_action_ext_accepted_with_time = 0
        n_action_majority_against = 0
        rates_ext = []
        rates_ord = []
        # Loop on posts
        for post in posts:
            # Count people
            if someone_join(post):
                people_count += 1
                # if command == "blame":
                #     new_user_id = get_new_user_id(mm_driver, post)
                #     if
                #     users[new_user_id] =
                continue
            if someone_leave(post):
                people_count -= 1
                # print(get_leaving_user_id(mm_driver, post))
                continue

            # Check if it's a vote. Ignore other messages
            if not is_vote(post):
                continue
            # Drop messages without reactions
            if "reactions" not in post["metadata"].keys():
                continue

            n_actions = n_actions + 1
            # Counters for the vote
            for_count = 0
            against_count = 0
            neutral_count = 0
            voters = []

            for reaction in post["metadata"]["reactions"]:
                # If the user already votes, ignore him
                if reaction["user_id"] in voters:
                    if args.debug:
                        user_name = get_author_name(mm_driver, reaction['user_id'])
                        print('Ignoring emoji ' + reaction["emoji_name"] +
                              ' from ' + user_name + ' who already votes.')
                    continue

                # Count votes
                if reaction["emoji_name"] in FOR_EMOJIS:
                    for_count += 1
                    voters.append(reaction["user_id"])
                    continue
                elif reaction["emoji_name"] in AGAINST_EMOJIS:
                    against_count += 1
                    voters.append(reaction["user_id"])
                    continue
                elif reaction["emoji_name"] in NEUTRAL_EMOJIS:
                    neutral_count += 1
                    voters.append(reaction["user_id"])
                    continue
                else:
                    if args.debug:
                        user_name = get_author_name(mm_driver, reaction['user_id'])
                        print('Ignoring emoji ' + reaction["emoji_name"] +
                              ' from ' + user_name + ' which is not a valid vote.')
                    continue

            against_with_no_majority = against_count and for_count * 2 < people_count
            if against_count * 2 > people_count: 
                n_action_majority_against = n_action_majority_against + 1
            if against_count > 0:
                n_action_against = n_action_against + 1
            if against_with_no_majority:
                n_action_against_with_no_majority = n_action_against_with_no_majority + 1
            if neutral_count > 0:
                n_action_neutral = n_action_neutral + 1
            if ACTION_EXTRAORDINAIRE in post['message']:
                n_action_ext = n_action_ext + 1
                if for_count * 2 <= people_count and not against_count:
                    n_action_ext_accepted_with_time = n_action_ext_accepted_with_time + 1
                rates_ext.append(for_count / people_count)
            elif ACTION_ORDINAIRE in post['message']:
                n_action_ord = n_action_ord + 1
                rates_ord.append(for_count / people_count)
            elif args.debug:
                print("⚠️ Hashtag ordinaire ou extraordinaire non trouvé : à corriger en éditant le message.")
            voters_count = len(voters)

            # Process result for differents command
            if command == "stats":
                act_type = 'EXT' if ACTION_EXTRAORDINAIRE in post['message'] else 'ORD'
                print(f"{'❓' if against_count else '✅'} \
[{datetime.utcfromtimestamp(post['create_at']/1000).strftime('%d-%m-%Y')}] [{act_type}] \
{voters_count*100/people_count:2.0f}% ({len(voters):2.0f}/{people_count:2.0f}) → \
https://team.picasoft.net/picasoft/pl/{post['id']} \
« {get_main_message(post['message'], 50).replace(linesep, ' ')} »")

            elif command == "export":
                # Determine the leader of the vote
                user_name = get_author_name(mm_driver, post["user_id"])

                # Determine if the action is extraordinary and append the new line to the propper file if it is
                if re.match(rf".*{ACTION_EXTRAORDINAIRE}.*", get_main_message(post["message"])):
                    extraordinary_count = extraordinary_count + 1
                    line = "| " + " #" + str(extraordinary_count) +  \
                        " | " + post["message"].replace('\n', ' ').replace('\u22c5', ' ') + \
                        " | " + user_name + \
                        " | " + datetime.utcfromtimestamp(post['create_at']/1000).strftime("%d-%m-%Y") + \
                        " | " + str(for_count) + "/" + str(voters_count) + \
                        " | " + str(against_count) + "/" + str(voters_count) + \
                        " | " + "[Lien](" + \
                        "https://" + MATTERMOST_DOMAIN + "/" + MATTERMOST_TEAM + "/pl/" + str(post["id"]) + ") |\n"
                    with open(EXTRAORDINARY_OUTPUT_FILE, "a") as file_object:
                        file_object.write(line)

                # Determine if the action is ordinary and append the new line to the propper file if it is
                if re.match(rf".*{ACTION_ORDINAIRE}.*", get_main_message(post["message"])):
                    ordinary_count = ordinary_count + 1
                    line = "| " + " #" + str(ordinary_count) +  \
                        " | " + post["message"].replace('\n', ' ').replace('\u22c5', ' ') + \
                        " | " + user_name + \
                        " | " + datetime.utcfromtimestamp(post['create_at']/1000).strftime("%d-%m-%Y") + \
                        " | " + str(for_count) + "/" + str(voters_count) + \
                        " | " + str(against_count) + "/" + str(voters_count) + \
                        " | " + "[Lien](" + \
                        "https://" + MATTERMOST_DOMAIN + "/" + MATTERMOST_TEAM + "/pl/" + str(post["id"]) + ") | \n"
                    with open(ORDINARY_OUTPUT_FILE, "a") as file_object:
                        file_object.write(line)

        print('=======================')
        print(f'Total des actions : {n_actions} ({n_action_ord} ordinaires, {n_action_ext} extraordinaires)')
        print(f'Taux d\'actions avec au moins un neutre : {int((n_action_neutral * 100) / n_actions)}%')
        print(f'Taux d\'actions avec au moins un contre : {int((n_action_against * 100) / n_actions)}%')
        print(f'Taux d\'actions avec une majorité contre : {int((n_action_majority_against * 100) / n_actions)}%')
        print(f'Taux d\'actions extraordinaires sans vote contre acceptées par délai : {int((n_action_ext_accepted_with_time / n_action_ext) * 100)}%')
        print(f'Taux d\'actions avec au moins un contre acceptées par délai : {int((n_action_against_with_no_majority * 100) / n_actions)}%')
        print(f'Taux moyen de participation aux actions ordinaires : {int(mean(rates_ord) * 100)}%')
        print(f'Taux moyen de participation aux actions extraordinaires : {int(mean(rates_ext) * 100)}%')

        if n_action_ord + n_action_ext != n_actions:
            print("⚠️ La somme des actions ordinaires et extraordinaires est en dessous de la somme des actions.")
            print("Relancez avec l'option debug pour voir d'où vient le problème.")
if __name__ == '__main__':
    main()
